
/* 
Copyright 2012,2018 Doni Pracner

This program is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public
License along with this program. If not, see
<http://www.gnu.org/licenses/>.

*/
import java.io.*;
import java.util.*;

/**
 * This program converts files from assembly x86 to WSL language which is a part
 * of the FermaT Transformation system.<br />
 * Currently it works only on a very limited subset of x86, and has a number of
 * predefined macro names it uses for translation. It works with 16 bit
 * registers (ax,bx, etc) and presumes that the rest of the variables are 8bit
 * if not applied to a 16bit reg. Check the documentation for more details.
 *
 * @author Doni Pracner, http://perun.dmi.rs/pracner http://quemaster.com
 */
public class asm2wsl {

	private static final String PROCEDURE_INTERNAL_SYS = "procedure_internal_sys";

	/** translation processing switch */
	public boolean originalInComments = false, translateComments = true,
			addMemoryDump = false, translateToPopPush = true;

	int mode = MODE_0, nUnknownC = 0, nUnknownD = 0, nKnownC = 0;

	boolean labelfound = false;

	public static final String versionN = "0.81";

	// regular comments from the original file
	// OC when original code is inserted in the file, next to the translations
	// SPEC special messages from the translator
	// ERR error messages from the translator
	public static final char C_REG = ' ', C_OC = '#', C_SPEC = '&', C_ERR = '!';

	public static final int MODE_0 = 0, MODE_CODE = 1, MODE_DATA = 2,
			MODE_STACK = 3, N_MODES = 4;

	public static final String CODE_SEG_START = "SKIP;\n ACTIONS A_S_start:\n A_S_start ==",
			CODE_SEG_END = "CALL Z;\nSKIP END\nENDACTIONS;\n",
			DATA_SEG_START = "VAR< ", DATA_SEG_END = " \tskipvar := 0 \n>:\n",
			fixl = "\n%x := (%x DIV 256) * 256 + t_e_m_p;",
			fixh = "\n%x := (%x MOD 256) + t_e_m_p * 256;";

	public String getStandardStart() {
		String ret = "C:\" This file automatically converted from assembler\";\n"
				+ "C:\" with asm2wsl v " + versionN + "\";\n";
		ret += "VAR < ax := 0, bx := 0, cx := 0, dx := 0,\n"
				+ "	si := 0, di := 0, bp := 0, sp := 0, \n"
				+ "	ss := 0, ds := 0, cs := 0, es := 0,\n"
				+ "	flag_o := 0, flag_d := 0, flag_i := 0, flag_t := 0,\n"
				+ "	flag_z := 0, flag_s := 0, flag_p := 0, flag_a := 0,\n"
				+ "	flag_c := 0, overflow := 256, stack := < >, t_e_m_p := 0 > :\n";
		if (addMemoryDump)
			ret += "BEGIN\n";
		return ret;
	}

	public String getStandardEnd() {
		String ret = "ENDVAR;\n";
		if (addMemoryDump) {
			ret += "C:\" end original program. testing features following\";\n"
					+ "WHERE\n" + "PROC memdump(VAR) ==\n"
					+ "	PRINT(\"'memory' dump: \");\n"
					+ "	PRINT(\"ax \",ax,\" bx \",bx,\" cx \",cx,\" dx \",dx);\n"
					+ "	PRINT(\"flags:\");\n" + "	PRINT(\"ODITSZAPC\");\n"
					+ "	PRINT(flag_o,flag_d,flag_i,flag_t,flag_s,flag_z,flag_a,flag_p,flag_c)\n"
					+ "END\n" + "END\n";
		}
		ret += "\nENDVAR";
		return ret;
	}

	/**
	 * When the param works with array addressing, this should reformat it so
	 * that it works as intended. An important thing to remember is that there
	 * is a[0] element in WSL, all arrays start with a[1].
	 */
	public String arrayFix(String str) {
		String param = str.substring(str.indexOf('[') + 1, str.indexOf(']'));
		return str.replace(param + "]", formatParam(param) + "+1]");
	}

	/**
	 * Transforms an asembler source parameter as need so that it can be used in
	 * WSL. Among other things it converts Hex numbers to decimal, fixes array
	 * access indexes and the high/low parts of registers.
	 */
	public String formatParam(String str) {
		str = str.toLowerCase();
		if (str.contains("["))
			str = arrayFix(str);
		try {
			if (str.matches("[0-9][0-9a-f]*h"))
				return "" + Integer.parseInt(str.substring(0, str.length() - 1),
						16);
		} catch (Exception e) {
			System.err.println("error in hex param procesing");
			e.printStackTrace();
		}
		if (str.compareTo("al") == 0)
			return "(ax MOD 256)";
		else if (str.compareTo("ah") == 0)
			return "(ax DIV 256)";
		else if (str.compareTo("bl") == 0)
			return "(bx MOD 256)";
		else if (str.compareTo("bh") == 0)
			return "(bx DIV 256)";
		else if (str.compareTo("cl") == 0)
			return "(cx MOD 256)";
		else if (str.compareTo("ch") == 0)
			return "(cx DIV 256)";
		else if (str.compareTo("dl") == 0)
			return "(dx MOD 256)";
		else if (str.compareTo("dh") == 0)
			return "(dx DIV 256)";
		else
		// for undefined variables
		if (str.compareTo("(?)") == 0)
			return "\" \"";
		else

			return str;
	}

	/**
	 * Transforms an asembler destination parameter as need so that it can be
	 * used in WSL. Among other things it fixes array access indexes and the
	 * high/low parts of registers.
	 */
	public String formatDst(String str) {
		if (str.contains("["))
			str = arrayFix(str);
		str = str.toLowerCase();
		if (str.length() == 2 && str.charAt(0) >= 'a' && str.charAt(0) <= 'd'
				&& (str.charAt(1) == 'l' || str.charAt(1) == 'h'))
			return "t_e_m_p";
		else
			return str;
	}

	/**
	 * Used for fixing low/high register access. The given param should be the
	 * dst operand of an assembler command.
	 */
	public String getXRegisterFix(String str) {
		str = str.toLowerCase();
		if (str.length() == 2 && str.charAt(0) >= 'a' && str.charAt(0) <= 'd') {
			char c = str.charAt(0);
			if (str.charAt(1) == 'l')
				return fixl.replace('%', c);
			if (str.charAt(1) == 'h')
				return fixh.replace('%', c);
		}
		return "";
	}

	/**
	 * Creates a WSL comment with care to quote chars.
	 */
	public static String createComment(String str) {
		return createComment(str, C_REG);
	}

	/**
	 * Creates a WSL comment with care to quote chars, of the given type. Types
	 * are given as char constants. They can be default comments, comments that
	 * contain the original code in them, or additional comments regarding the
	 * translation process.
	 */
	public static String createComment(String str, char type) {
		return "C:\"" + type + str.replace("\"", "''") + "\";";
	}

	/**
	 * Creates a wsl statement that modifies the overflow limit based on the
	 * input string that should be the parameter of a command.
	 */
	public String getOverflow(String str) {
		int over = 256;
		str = str.toLowerCase();
		if (str.length() == 2) {
			if ((str.charAt(1) == 'x' && str.charAt(0) >= 'a'
					&& str.charAt(0) <= 'd')
					|| (str.compareTo("si") == 0 || str.compareTo("bp") == 0))
				over = 65536;
		}
		return "overflow := " + over + "; ";
	}

	private void debugPrint(ArrayList<String> com) {
		System.out.println();
		for (int i = 0; i < com.size(); i++) {
			System.out.print(com.get(i));
			System.out.print("||");
		}
	}

	private boolean inMacro = false;

	// internal state, if a procedure body is being translated currently
	private boolean inProcedure = false;

	// the name of the procedure currently translated, if there is one
	private String currentProcName = "";

	/**
	 * Takes a single assembler line and returns the translated WSL line(s). It
	 * also changes the translator modes (data, code etc).
	 */
	public String processLine(String str) {
		/* Most of the logic is here, some parts could be split off */
		if (str.length() == 0)
			return "";
		String[] split = str.split(";", 2);
		String rez = split[0];
		String comment = "";
		// removes the comments
		if (translateComments && split.length > 1)
			comment = createComment(split[1]);
		if (rez.compareTo("") != 0) {
			// if there is a non-comment part
			String[] parts = rez.split("[\t ,]");
			// separators are whitespace: tab, space and comma
			ArrayList<String> com = new ArrayList<String>();
			for (String s : parts)
				if (s.compareTo("") != 0 && s != null)
					com.add(s);
			rez = "";
			String a, b;
			// special case, we are processing lines in a macro definition
			if (inMacro) {
				Iterator<String> it = com.iterator();
				while (it.hasNext())
					if (it.next().compareTo("endm") == 0)
						inMacro = false;
				return createComment(str, C_SPEC) + "\n";
			}
			// by this time, all the real words should be in the com array
			int i = 0;
			boolean work = true;
			// debugPrint(com);
			while (i < com.size() && work) {
				String s = com.get(i).toLowerCase();
				if (s.endsWith(":")) { // should represent a label
					s = s.substring(0, s.length() - 1);
					rez += "CALL " + s + "\n END\n";
					rez += s + " == ";
					// labelfound = true;
				} else
				// ------------------------------------------------------------
				// procedures, calls and similar activities
				if (s.compareTo("proc") == 0) {
					// if (labelfound)
					rez += "CALL Z; SKIP END \n";
					a = com.get(i - 1);
					rez += a + " == ";
					b = PROCEDURE_INTERNAL_SYS;
					rez += "\nACTIONS " + b + ": \n";
					rez += b + " == ";
					inProcedure = true;
					currentProcName = a;
				} else if (s.compareTo("endp") == 0) {
					rez += " SKIP END\n ENDACTIONS\n END\n dummy" + nKnownC
							+ " == ";
					inProcedure = false;
				} else if (s.compareTo("ret") == 0) {
					rez += " CALL Z;";
				} else if (s.compareTo("call") == 0) {
					a = com.get(++i);
					// check for recursive calls, don't do outside of current 
					// action system
					if (inProcedure && currentProcName.equals(a))
						a = PROCEDURE_INTERNAL_SYS;
					rez += "CALL " + a + ";";
				} else
				// segments and other special assembler directives
				if (s.charAt(0) == '.') { //
					if (s.compareTo(".code") == 0) {
						rez = CODE_SEG_START;
						mode = MODE_CODE;
					} else if (s.compareTo(".data") == 0) {
						rez = " ";
						mode = MODE_DATA;
					} else if (s.compareTo(".stack") == 0) {
						rez = " ";
						mode = MODE_STACK;
					} else if (s.startsWith(".model")) {
						if (!s.toLowerCase().endsWith("small")) {
							rez += createComment(
									"other models than small not supported :: "
											+ split[0],
									C_SPEC);
							nUnknownD++;
							mode = MODE_0;
						}
					} else {
						rez += createComment(
								"directive not supported :: " + split[0],
								C_SPEC);
						nUnknownD++;
						mode = MODE_0;
					}
					work = false;
				} else if (s.compareTo("assume") == 0) {
					rez += createComment(
							"ASSUME commands not supported :: " + split[0],
							C_SPEC);
					nUnknownD++;
					work = false;
				} else if (s.compareTo("segment") == 0) {
					String segn = com.get(i - 1).toLowerCase();
					if (segn.compareTo("cseg") == 0
							|| segn.compareTo("code") == 0
							|| segn.compareTo("code_seg") == 0) {
						rez += CODE_SEG_START;
						mode = MODE_CODE;
					} else if (segn.compareTo("dseg") == 0
							|| segn.compareTo("data_seg") == 0
							|| segn.compareTo("data") == 0) {
						rez += " ";
						mode = MODE_DATA;
					} else if (segn.compareTo("sseg") == 0
							|| segn.compareTo("stack_seg") == 0
							|| segn.compareTo("stack") == 0) {
						rez += " ";
						mode = MODE_STACK;
					} else {
						rez += createComment(
								"unsuported segment type :: " + split[0],
								C_SPEC);
						nUnknownD++;
						mode = MODE_0;
					}
					work = false;
				} else if (s.compareTo("ends") == 0) { // segment end
					if (i > 0) {
						String segn = com.get(i - 1).toLowerCase();
						if (segn.compareTo("cseg") == 0
								|| segn.compareTo("code_seg") == 0
								|| segn.compareTo("code") == 0
								|| segn.compareTo("dseg") == 0
								|| segn.compareTo("data") == 0
								|| segn.compareTo("data_seg") == 0
								|| segn.compareTo("sseg") == 0
								|| segn.compareTo("stack") == 0
								|| segn.compareTo("stack_seg") == 0) {
							rez += createComment("end of segment " + segn,
									C_SPEC);
							mode = MODE_0;
						} else {
							rez += createComment(
									"unsuported segment type :: " + split[0],
									C_SPEC);
							nUnknownD++;
						}
					} else {
						rez += createComment("end of segment", C_SPEC);
						mode = MODE_0;
					}
					work = false;
				} else
				// macro ideas ----
				if (s.compareTo("macro") == 0) {
					rez += createComment(
							"found macro definition - macros unsuported",
							C_ERR);
					nUnknownC++;
					inMacro = true;
					work = false;
				} else if (s.compareTo("endm") == 0) {
					rez += createComment(
							"found macro definition end - macros unsuported",
							C_ERR);
					nUnknownC++;
					work = false;
				} else

				/*
				 * ------------------------------
				 *  
				 * special macro names translated directly
				 */
				if ((s.compareTo("print_str") == 0
						|| s.compareTo("print_num") == 0
						|| s.compareTo("print_char") == 0)
						&& com.get(i + 1).compareTo("macro") != 0) { 
					rez += "PRINFLUSH(" + com.get(i + 1) + ");";

					work = false;
				} else if (s.compareTo("print_new_line") == 0
						&& (com.size() <= i + 1
								|| com.get(i + 1).compareTo("macro") != 0)) { 
					rez += "PRINT(\"\");";

					work = false;
				} else if (s.compareTo("read_str") == 0
						&& com.get(i + 1).compareTo("macro") != 0) { 
					rez += "@Read_Line_Proc(VAR " + com.get(i + 1)
							+ ", Standard_Input_Port);";

					work = false;
				} else if (s.compareTo("read_num") == 0
						&& com.get(i + 1).compareTo("macro") != 0) { 
					rez += "@Read_Line_Proc(VAR t_e_m_p, Standard_Input_Port);\n";
					rez += com.get(i + 1) + " := @String_To_Num(t_e_m_p);";

					work = false;
				} else if (s.compareTo("end_execution") == 0
						&& (com.size() <= i + 1
								|| com.get(i + 1).compareTo("macro") != 0)) { 
					rez += "CALL Z;";

					work = false;
				} else

				// ---------------------------------------
				// operators and similar commands
				if (s.compareTo("mov") == 0) {
					a = com.get(i + 1);
					b = com.get(i + 2).toLowerCase();
					if ((b.compareTo("@data") == 0)
							|| (b.compareTo("dseg") == 0)) {
						rez += createComment("unneeded DATA segment fixes",
								C_SPEC);
						work = false;
					} else {// rez += getOverflow(a);
						rez += formatDst(a) + " := " + formatParam(b) + ";";
						rez += getXRegisterFix(a);
						work = false;
					}
				} else if (s.compareTo("add") == 0 || s.compareTo("adc") == 0) {
					a = com.get(i + 1);
					b = com.get(i + 2);
					rez += getOverflow(a);
					rez += formatDst(a) + " := " + formatParam(a) + " + "
							+ formatParam(b) + ";\n";
					if (s.compareTo("adc") == 0)
						rez += formatDst(a) + " := " + formatParam(a) + " + "
								+ " + flag_c;\n";
					rez += "IF " + formatDst(a) + " >= overflow THEN "
							+ formatDst(a) + " := " + formatDst(a)
							+ " MOD overflow; flag_o :=1; flag_c := 1; ELSE flag_o :=0; flag_c := 0; FI;";
					rez += getXRegisterFix(a);
					work = false;
				} else if (s.compareTo("sub") == 0 || s.compareTo("cmp") == 0) {
					a = com.get(i + 1);
					b = com.get(i + 2);
					// rez += getOverflow(a);
					rez += "IF " + formatParam(a) + " = " + formatParam(b)
							+ " THEN\n\t flag_z := 1\n ELSE\n\t flag_z := 0\n FI; ";
					rez += "IF " + formatParam(a) + " < " + formatParam(b)
							+ " THEN\n\t flag_c := 1\n ELSE\n\t flag_c := 0\n FI; ";
					if (s.compareTo("sub") == 0)
						rez += formatDst(a) + " := " + formatParam(a) + " - "
								+ formatParam(b) + ";";
					work = false;
				} else if (s.compareTo("inc") == 0) {
					a = com.get(i + 1);
					rez += getOverflow(a);
					rez += formatDst(a) + " := " + formatParam(a) + " + 1;";
					rez += getXRegisterFix(a);
					work = false;
				} else if (s.compareTo("dec") == 0) {
					a = com.get(i + 1);
					rez += getOverflow(a);
					rez += formatDst(a) + " := " + formatParam(a) + " - 1;";
					rez += getXRegisterFix(a);
					work = false;
				} else if (s.compareTo("shr") == 0) {
					a = com.get(i + 1);
					// rez += getOverflow(a);
					rez += "IF " + formatParam(a) + " MOD 2 = 1 THEN "
							+ " flag_c := 1; ELSE flag_c := 0; FI;";
					rez += formatDst(a) + " := " + formatParam(a) + " DIV 2;";
					rez += getXRegisterFix(a);
					work = false;
				} else if (s.compareTo("shl") == 0) {
					a = com.get(i + 1);
					rez += getOverflow(a);
					rez += formatDst(a) + " := " + formatParam(a) + " * 2;";
					rez += "IF " + formatDst(a) + " >= overflow THEN "
							+ formatDst(a) + " := " + formatDst(a)
							+ " MOD overflow; flag_o :=1; flag_c := 1; ELSE flag_o :=0; flag_c := 0; FI;";
					rez += getXRegisterFix(a);
					work = false;
				} else if (s.compareTo("xchg") == 0) {
					a = com.get(i + 1);
					b = com.get(i + 2);
					rez += "< " + formatDst(a) + " := " + formatParam(b) + ", "
							+ formatDst(b) + " := " + formatParam(a) + " >;";
					rez += getXRegisterFix(a);
					work = false;
				} else
				// logical exp
				if (s.compareTo("and") == 0) {
					// rez += com.get(i+1)+" := "+com.get(i+1)+" AND
					// "+formatParam(com.get(i+2))+";";
					rez += createComment(
							"and not implemented yet :: " + split[0], C_ERR);
					nUnknownC++;
					work = false;
				} else if (s.compareTo("xor") == 0) {
					a = com.get(i + 1);
					b = formatParam(com.get(i + 2));
					if (a.compareTo(b) == 0)
						rez += a + " := 0;";
					else {
						rez += createComment(
								"xor not implemnted yet :: " + split[0], C_ERR);
						// a+" := ("+a+" AND NOT "+b+") OR (NOT "+a+" AND "+b+"
						// );");
						nUnknownC++;
					}
					work = false;
				} else if (s.compareTo("not") == 0) {
					// rez += com.get(i+1)+" := NOT "+com.get(i+1)+";";
					rez += createComment(
							"NOT not implemented yet :: " + split[0], C_ERR);
					nUnknownC++;
					work = false;
				} else
				// jumps
				if (s.compareTo("jmp") == 0) {
					rez += "CALL " + com.get(i + 1) + ";";
					work = false;
				} else if (s.compareTo("je") == 0 || s.compareTo("jz") == 0) {
					rez += "IF flag_z = 1 THEN CALL " + com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("jne") == 0 || s.compareTo("jnz") == 0) {
					rez += "IF flag_z = 0 THEN CALL " + com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("ja") == 0) {
					rez += "IF flag_z = 0 AND flag_c = 0 THEN CALL "
							+ com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("jae") == 0 || s.compareTo("jnc") == 0) {
					rez += "IF flag_c = 0 THEN CALL " + com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("jb") == 0) {
					rez += "IF flag_c = 1 THEN CALL " + com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("jcxz") == 0) {
					rez += "IF cx = 0 THEN CALL " + com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("jo") == 0) {
					rez += "IF flag_o = 1 THEN CALL " + com.get(i + 1) + " FI;";
					work = false;
				} else if (s.compareTo("loop") == 0) {
					rez += "cx := cx - 1;\n";
					rez += "IF cx>0 THEN CALL " + com.get(i + 1)
							+ " ELSE CALL dummy" + nKnownC + " FI\n";
					rez += "END\ndummy" + nKnownC + " ==";
					work = false;
				} else

				// end and other
				if (s.compareTo("end") == 0) {
					// TODO parse the optional entry label that comes with end
					rez += createComment("program end");
					work = false;
				} else if (s.compareTo("nop") == 0) {
					rez += "SKIP;";
					work = false;
				} else

				// stack
				if (s.compareTo("pop") == 0) {
					if (translateToPopPush) {
						rez += "POP(" + formatDst(com.get(i + 1)) + ", stack);";
					} else {
						rez += formatDst(com.get(i + 1)) + " := HEAD(stack);\n";
						rez += "stack := TAIL(stack);";
					}
					rez += getXRegisterFix(com.get(i + 1));
					work = false;
				} else if (s.compareTo("push") == 0) {
					if (translateToPopPush) {
						rez += "PUSH(stack, " + formatParam(com.get(i + 1))
								+ ");";
					} else {
						rez += "stack := < " + formatParam(com.get(i + 1))
								+ " > ++ stack;";
					}
					work = false;
				} else
				// var definitions
				if (s.compareTo("db") == 0 || s.compareTo("dw") == 0) {
					if (mode == MODE_DATA) {
						if (com.size() == i + 2) {
							rez += "\t " + com.get(i - 1) + " := "
									+ formatParam(com.get(i + 1)) + ",";
						} else {// array
							rez += "\t " + com.get(i - 1) + " := < "
									+ formatParam(com.get(i + 1));
							for (int j = i + 2; j < com.size(); j++)
								rez += "," + formatParam(com.get(j));
							rez += " >,";
						}
					} else {

					}
				}
				i++;
			}
			if (rez.compareTo("") == 0 && com.size() > 0) {
				rez = createComment(" unkown command:" + split[0], C_ERR);
				nUnknownC++;
			} else
				nKnownC++; // this now counts the directives too...
		}
		return rez + " " + comment + "\n";

	}

	/**
	 * Converts an asm file to a wsl file. It also prints out the number of
	 * unsupported commands.
	 */
	public void convertAFile(File f) {
		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader(new FileInputStream(f)));
			String str = in.readLine(), rez = f.getPath();
			if (rez.toLowerCase().endsWith(".asm"))
				rez = rez.substring(0, rez.length() - 4);
			PrintWriter out = new PrintWriter(
					new OutputStreamWriter(new FileOutputStream(rez + ".wsl")));
			nUnknownD = 0;
			nUnknownC = 0;
			nKnownC = 0;
			StringBuilder[] sbs = new StringBuilder[N_MODES];
			for (int i = 0; i < N_MODES; i++)
				sbs[i] = new StringBuilder();
			while (str != null) {
				rez = processLine(str);
				if (originalInComments)
					sbs[mode].append(createComment(str, C_OC) + "\n");
				sbs[mode].append(rez);
				str = in.readLine();
			}
			sbs[MODE_CODE].append(CODE_SEG_END);
			if (addMemoryDump)
				sbs[MODE_CODE].append("memdump();");
			sbs[MODE_DATA].append(DATA_SEG_END);
			// remove comments from dataseg
			String datastr = sbs[MODE_DATA].toString()
					.replaceAll("C:\"(.*?)\";\n?", "");
			in.close();
			out.print(getStandardStart());
			out.print(sbs[MODE_0]);
			out.print(DATA_SEG_START);
			out.print(datastr); // out.print(sbs[MODE_DATA]);
			out.print(sbs[MODE_CODE]);
			out.print(getStandardEnd());
			out.close();
			if (nUnknownD > 0)
				System.out.println("warning: " + nUnknownD
						+ " unkown/unsuported directives were found");
			if (nUnknownC > 0)
				System.out.println("errors : " + nUnknownC
						+ " unkown/unsuported commands were found");
			System.out.println(nKnownC + " commands translated");
		} catch (Exception e) {
			System.err.println("error in conversion:");
			e.printStackTrace();
		}
	}

	public void run(String[] args) {
		if (args.length == 0) {
			System.out.println("Assembler to WSL converter. v " + versionN
					+ ", by Doni Pracner");
			System.out.println("usage:\n\t asm2wsl {-option[ +-]} filename");
			System.out.println("options: (def value in parenthesis)");
			System.out.println("\t -oc : orignal code in comments ("
					+ (originalInComments ? "+" : "-") + ")");
			System.out.println("\t -c : translate comments ("
					+ (translateComments ? "+" : "-") + ")");
			System.out.println("\t -dump : add memory dump commands to end ("
					+ (addMemoryDump ? "+" : "-") + ")");
		} else {
			if (args[0].compareTo("-h") == 0
					|| args[0].compareTo("--help") == 0) {
				run(new String[0]);
				return;
			}
			int i = 0;
			while (i < args.length && args[i].charAt(0) == '-') {
				if (args[i].startsWith("-oc")) {
					if (args[i].length() == 4)
						originalInComments = args[i].charAt(3) == '+';
					else
						originalInComments = true;
				} else if (args[i].startsWith("-c")) {
					if (args[i].length() == 3)
						translateComments = args[i].charAt(2) == '+';
					else
						translateComments = true;
				} else if (args[i].startsWith("-dump")) {
					addMemoryDump = true;
					if (args[i].length() == 6 && args[i].charAt(5) == '-')
						addMemoryDump = false;
				}
				i++;
			}

			if (i >= args.length) {
				System.out.println("no filename supplied");
				System.exit(2);
			}
			File f = new File(args[i]);
			if (f.exists()) {
				Calendar now = Calendar.getInstance();
				convertAFile(f);
				long mili = Calendar.getInstance().getTimeInMillis()
						- now.getTimeInMillis();
				System.out.println("conversion time:" + mili + " ms");
			} else
				System.out.println("file does not exist");
		}
	}

	public static void main(String[] args) {
		new asm2wsl().run(args);
	}
}
