.model small
;make a sum of an array - version with predefined everything

;#macro-commands-start
end_execution macro
; finish the execution
    mov ax, 4c02h
    int 21h
endm 

print_str macro s
; print a s string on screen
    push ax
    push dx  
    mov dx, offset s
    mov ah, 09
    int 21h
    pop dx
    pop ax
endm

inttostr macro num1 str1
; convert num1 to str1
   push ax
   push bx
   push cx
   push dx
   push si
   mov ax, num1
   mov dl, '$'
   push dx
   mov si, 10
itosloop:
   mov dx, 0
   div si
   add dx, 48
   push dx
   cmp ax, 0
   jne itosloop
   
   mov bx, offset str1
itosloopa:      
   pop dx
   mov [bx], dl
   inc bx
   cmp dl, '$'
   jne itosloopa
   pop si  
   pop dx
   pop cx
   pop bx
   pop ax 
endm

print_num macro num
    ;convert num, print str
    inttostr num,tempStr
    print_str tempStr
endm
;#macro-commands-end

.code
start:
        mov dx, @data
        mov ds, dx 
        mov cx, n
        mov ax, 0
        mov dx, 0
mainloop:              
        mov bx, cx
        add al, niz[bx-1]   ; get array memeber, byte
        ; store sum in al
        loop mainloop         ; end calc if done
        print_num ax
        end_execution
               
data segment
n       dw  7
niz     db  1,2,3,4,5,6,7,0     
tempStr db "        "

ends
end start
