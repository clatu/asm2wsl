.model      small     
;recursive version of GCD, predefined input
data segment
  strNZD db "        "
data ends
;#macro-commands-start
; print a string on screen
print_str macro s
    push ax
    push dx  
    mov dx, offset s
    mov ah, 09
    int 21h
    pop dx
    pop ax
endm


; write a single char
print_char macro c
    push ax   
    push dx
    mov ah, 02
    mov dl, c
    int 21h
    pop dx
    pop ax
endm

; finish the execution
end_execution macro
    mov ax, 4c02h
    int 21h
endm 

; Konvertovanje broja u string
inttostr macro num1 str1
   push ax
   push bx
   push cx
   push dx
   push si
   mov ax, num1
   mov dl, '$'
   push dx
   mov si, 10
petlja2:
   mov dx, 0
   div si
   add dx, 48
   push dx
   cmp ax, 0
   jne petlja2
   
   mov bx, offset str1
petlja2a:      
   pop dx
   mov [bx], dl
   inc bx
   cmp dl, '$'
   jne petlja2a
   pop si  
   pop dx
   pop cx
   pop bx
   pop ax 
endm

print_num macro num
	inttostr num,strNZD
	print_str strNZD
endm
;#macro-commands-end

.code
    
start:
    push 8
    push 12
    call gcd
    pop ax
	print_num ax
; print out a result
    end_execution
    
;volatile procedure, ruins ax,bx,cx
gcd proc
;#extra-start
    pop cx ;ret adress    
;#extra-end
    ;get params
    pop ax
    pop bx
;#extra-start
    push cx ;ret for later 
;#extra-end
    cmp ax,bx
    je endequal
    ja greatera
    ;ensure ax is greater
    xchg ax,bx          
greatera:
    sub ax,bx
    push bx
    push ax
    call gcd
    pop ax;result
    
endequal:
;#extra-start
    pop cx
;#extra-end
    push ax; result
;#extra-start
    push cx;needed before ret
;#extra-end
    ret
gcd endp
.stack
end start
