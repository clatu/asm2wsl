.model small
;make a sum of an array - version with loading values
;predefined array size

;#macro-commands-start
end_execution macro
; finish the execution
    mov ax, 4c02h
    int 21h
endm 

print_str macro s
; print a s string on screen
    push ax
    push dx  
    mov dx, offset s
    mov ah, 09
    int 21h
    pop dx
    pop ax
endm

inttostr macro num1 str1
; convert num1 to str1
   push ax
   push bx
   push cx
   push dx
   push si
   mov ax, num1
   mov dl, '$'
   push dx
   mov si, 10
itosloop:
   mov dx, 0
   div si
   add dx, 48
   push dx
   cmp ax, 0
   jne itosloop
   
   mov bx, offset str1
itosloopa:      
   pop dx
   mov [bx], dl
   inc bx
   cmp dl, '$'
   jne itosloopa
   pop si  
   pop dx
   pop cx
   pop bx
   pop ax 
endm

print_num macro num
    ;convert num, print str
    inttostr num,tempStr
    print_str tempStr
endm      

; new line
print_new_line macro
    push ax
    push bx
    push dx
    mov ah,03
    mov bh,0
    int 10h
    inc dh
    mov dl,0
    mov ah,02
    int 10h
    pop dx
    pop bx
    pop ax
endm

read_num macro num
        read_str tempStr,6
        strtoint tempStr,num
endm

;read into a buffer, inc the special 0 and 1 bytes
;fixes it into a $ terminated string
read_str macro strbuff, strlen
    LOCAL copystr
    push ax
    push bx
    push cx
    push dx
    push si
    mov bp, sp
    mov dx, offset strbuff
    mov bx, dx
    mov ax, strlen
    mov byte [bx] ,al
    mov ah, 0Ah
    int 21h
    mov si, dx     
    mov cl, [si+1] 
    mov ch, 0
copystr:
    mov al, [si+2]
    mov [si], al
    inc si
    loop copystr     
    mov [si], '$'
    pop si  
    pop dx
    pop cx
    pop bx
    pop ax
endm

;ascii to actual number
strtoint macro inStr,outNum
    LOCAL mainloop,end
    push ax
    push bx
    push cx
    push dx
    push si
    mov bp, sp
    mov bx, offset inStr
    mov ax, 0
    mov cx, 0
    mov si, 10
mainloop:
    mov cl, [bx]
    cmp cl, '$'
    je end
    mul si
    sub cx, 48
    add ax, cx
    inc bx  
    jmp mainloop
end:
    mov outNum, ax 
    pop si  
    pop dx
    pop cx
    pop bx
    pop ax
endm
;#macro-commands-end

.code
start:
        mov dx, @data
        mov ds, dx 
        print_str prompt
        read_num n
        mov ax, 0
        mov cx, n
        cmp maxn, cx
        jae normal
        print_str errorN
        end_execution
normal:          
loadloop:
        print_str prompt
        read_num t
        mov ax,t
        mov bx,n
        sub bx,cx
        mov niz[bx],al
        loop loadloop
        print_new_line
        ; now sum up
        mov cx, n
        xor ax,ax
mainloop:              
        mov bx, cx
        add al, niz[bx-1]   ; get array memeber, byte
        ; store sum in al
        loop mainloop         ; end calc if done
        print_num ax
        end_execution

data segment
n       dw  7
t       dw  0
maxn    dw  20
niz     db  1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0     
tempStr db "        "
prompt db "Number?$"
errorN db "Number too large!$"
ends
end start




