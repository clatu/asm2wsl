.model      small
;#macro-commands-start
; finish the execution
end_execution macro
    int 20h
endm                      

; print a string on screen
print_str macro s
    push ax
    push dx  
    mov dx, offset s
    mov ah, 09
    int 21h
    pop dx
    pop ax
endm
                    
; write a single char
print_char macro c
    push ax   
    push dx
    mov ah, 02
    mov dl, c
    int 21h
    pop dx
    pop ax
endm

; new line
print_new_line macro
    push ax
    push bx
    push dx
    mov ah,03
    mov bh,0
    int 10h
    inc dh
    mov dl,0
    mov ah,02
    int 10h
    pop dx
    pop bx
    pop ax
endm


; Konvertovanje broja u string
inttostr macro num1 str1
   push ax
   push bx
   push cx
   push dx
   push si
   mov ax, num1
   mov dl, '$'
   push dx
   mov si, 10
petlja2:
   mov dx, 0
   div si
   add dx, 48
   push dx
   cmp ax, 0
   jne petlja2
   
   mov bx, offset str1
petlja2a:      
   pop dx
   mov [bx], dl
   inc bx
   cmp dl, '$'
   jne petlja2a
   pop si  
   pop dx
   pop cx
   pop bx
   pop ax 
endm

print_num macro num
        inttostr num,tempStr
        print_str tempStr
endm

read_num macro num
	read_str tempStr,6
	;push offset tempStr
	;push offset num
	strtoint tempStr,num
endm

;read into a buffer, inc the special 0 and 1 bytes
;fixes it into a $ terminated string
read_str macro strbuff, strlen
    LOCAL copystr
    push ax
    push bx
    push cx
    push dx
    push si
    mov bp, sp
    mov dx, offset strbuff
    mov bx, dx
    mov ax, strlen
    mov byte [bx] ,al
    mov ah, 0Ah
    int 21h
    mov si, dx     
    mov cl, [si+1] 
    mov ch, 0
copystr:
    mov al, [si+2]
    mov [si], al
    inc si
    loop copystr     
    mov [si], '$'
    pop si  
    pop dx
    pop cx
    pop bx
    pop ax
endm

;ascii to actual number
strtoint macro inStr,outNum
    LOCAL mainloop,end
    push ax
    push bx
    push cx
    push dx
    push si
    mov bp, sp
    mov bx, offset inStr
    mov ax, 0
    mov cx, 0
    mov si, 10
mainloop:
    mov cl, [bx]
    cmp cl, '$'
    je end
    mul si
    sub cx, 48
    add ax, cx
    inc bx  
    jmp mainloop
end:
    mov outNum, ax 
    pop si  
    pop dx
    pop cx
    pop bx
    pop ax
endm
;#macro-commands-end

.code
;read_num, print_num, etc are macros

start:  
    mov ax, data
    mov ds, ax            
    print_str prompt
    read_num n
    mov cx, n
l1:                 
    print_str prompt
	read_num num
	push num
	loop l1
	; now sum up the top n from the stack
    mov cx, n
    mov ax, 0
    mov dx, 0
theloop:
    pop ax	          ; get next from stack
    add dx, ax        ; array sum is in dx
	loop theloop
	; result            
	print_new_line
	print_str resStr
	print_num dx
end1:
	nop
	end_execution


.data
	num dw 12
	n dw 3
	rez dw 0 
	tempStr db "        "
	prompt  db "number? $"
	resStr  db "result $"

.stack
end start
