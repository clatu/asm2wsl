.model      small
data segment
  strNZD db "        "
data ends
;#macro-commands-start
; print a string on screen
print_str macro s
    push ax
    push dx  
    mov dx, offset s
    mov ah, 09
    int 21h
    pop dx
    pop ax
endm


; write a single char
print_char macro c
    push ax   
    push dx
    mov ah, 02
    mov dl, c
    int 21h
    pop dx
    pop ax
endm

; finish the execution
end_execution macro
    mov ax, 4c02h
    int 21h
endm 

; Konvertovanje broja u string
inttostr macro num1 str1
   push ax
   push bx
   push cx
   push dx
   push si
   mov ax, num1
   mov dl, '$'
   push dx
   mov si, 10
petlja2:
   mov dx, 0
   div si
   add dx, 48
   push dx
   cmp ax, 0
   jne petlja2
   
   mov bx, offset str1
petlja2a:      
   pop dx
   mov [bx], dl
   inc bx
   cmp dl, '$'
   jne petlja2a
   pop si  
   pop dx
   pop cx
   pop bx
   pop ax 
endm

print_num macro num
	inttostr num,strNZD
	print_str strNZD
endm
;#macro-commands-end

.code      

start:
            mov   ax,12
            mov   bx,8

compare:    
            cmp   ax,bx
            je    exit ;exit since they're equal
            ja    greater
            sub   bx,ax
            jmp   compare

greater:
            sub   ax,bx
            jmp   compare

exit:
;exit out of the program
	print_num ax
; print out a result
            end_execution

.stack
end start
