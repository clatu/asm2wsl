.model      small
;#macros-removed-from-listing

.code
;read_num, print_num, etc are macros

start:  
    mov ax, data
    mov ds, ax            
    print_str prompt
    read_num n
    mov cx, n
l1:                 
    print_str prompt
	read_num num
	push num
	loop l1
	; now sum up the top n from the stack
    mov cx, n
    mov ax, 0
    mov dx, 0
theloop:
    pop ax	          ; get next from stack
    add dx, ax        ; array sum is in dx
	loop theloop
	; result            
	print_new_line
	print_str resStr
	print_num dx
end1:
	nop
	end_execution


.data
	num dw 12
	n dw 3
	rez dw 0 
	tempStr db "        "
	prompt  db "number? $"
	resStr  db "result $"

.stack
end start
