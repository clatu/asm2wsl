.model small
;make a sum of an array - version with loading values
;predefined array size

;#macros-removed-from-listing

.code
start:
        mov dx, @data
        mov ds, dx 
        print_str prompt
        read_num n
        mov ax, 0
        mov cx, n
        cmp maxn, cx
        jae normal
        print_str errorN
        end_execution
normal:          
loadloop:
        print_str prompt
        read_num t
        mov ax,t
        mov bx,n
        sub bx,cx
        mov niz[bx],al
        loop loadloop
        print_new_line
        ; now sum up
        mov cx, n
        xor ax,ax
mainloop:              
        mov bx, cx
        add al, niz[bx-1]   ; get array memeber, byte
        ; store sum in al
        loop mainloop         ; end calc if done
        print_num ax
        end_execution

data segment
n       dw  7
t       dw  0
maxn    dw  20
niz     db  1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0     
tempStr db "        "
prompt db "Number?$"
errorN db "Number too large!$"
ends
end start




