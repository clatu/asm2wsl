.model      small
;greatest common divisor, with input from terminal
data segment
  tempStr db "        "
  prompt  db "num?$"
  promptr db "RES $"
  m     dw  0
  n     dw  0

data ends
;#macros-removed-from-listing

.code      

start:           
    mov ax,@data
    mov ds,ax
    print_str prompt
    read_num m      
    print_new_line
    print_str prompt
    read_num n
    print_new_line

            mov   ax,n
            mov   bx,m

compare:    
            cmp   ax,bx
            je    exit ;exit since they're equal
            ja    greater
            sub   bx,ax
            jmp   compare

greater:
            sub   ax,bx
            jmp   compare

exit:
;exit out of the program
    mov n,ax
    print_str promptr
	print_num n
; print out a result
            end_execution

.stack
end start
