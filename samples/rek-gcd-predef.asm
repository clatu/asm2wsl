.model      small     
;recursive version of GCD, predefined input
data segment
  strNZD db "        "
data ends
;#macros-removed-from-listing

.code
    
start:
    push 8
    push 12
    call gcd
    pop ax
	print_num ax
; print out a result
    end_execution
    
;volatile procedure, ruins ax,bx,cx
gcd proc
    ;get params
    pop ax
    pop bx
    cmp ax,bx
    je endequal
    ja greatera
    ;ensure ax is greater
    xchg ax,bx          
greatera:
    sub ax,bx
    push bx
    push ax
    call gcd
    pop ax;result
    
endequal:
    push ax; result
    ret
gcd endp
.stack
end start
