.model      small     
ASSUME DS:data
;recursive version of GCD, input from terminal
data segment
  strNZD db "        "
  tempStr db "        "
  prompt  db "num?$"
  promptr db "RES $"
  m     dw  0
  n     dw  0
data ends
;#macros-removed-from-listing

.code

start:              
    mov ax,@data
    mov ds,ax
    print_str prompt
    read_num m      
    print_new_line
    print_str prompt
    read_num n
    print_new_line

    push n
    push m
    call gcd
    pop n    
    print_str promptr
	print_num n
; print out a result
    end_execution


;volatile procedure, ruins ax,bx,cx
gcd proc
	;get params
	pop ax
	pop bx
	cmp ax,bx
    	je endequal
	ja greatera
	;ensure ax is greater
	xchg ax,bx          
greatera:
	sub ax,bx
	push bx
	push ax
	call gcd
	pop ax;result
    
endequal:
	push ax; result
	ret
gcd endp 

.stack
end start
