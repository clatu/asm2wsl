.model      small
data segment
  strNZD db "        "
data ends
;#macros-removed-from-listing

.code      

start:
            mov   ax,12
            mov   bx,8

compare:    
            cmp   ax,bx
            je    exit ;exit since they're equal
            ja    greater
            sub   bx,ax
            jmp   compare

greater:
            sub   ax,bx
            jmp   compare

exit:
;exit out of the program
	print_num ax
; print out a result
            end_execution

.stack
end start
