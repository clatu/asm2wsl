.model      small
.code
;read_num, print_num are macros
        read_num n
	mov cx, n
l1:
	read_num num
	push num
	loop l1
	push n
	call sumn
	pop rez
	print_num rez
end1:
	nop

sumn	proc
;n is on top of the stack
;sum the next n top elements of the stack
	pop cx 
        mov ax, 0
        mov dx, 0
theloop:
        pop ax	          ; get next from stack
        add dx, ax        ; array sum is in dx
	loop theloop
	push dx           ; result
	    ret
sumn	endp

.data
	num dw 12
	n dw 3
	rez dw 0 
.stack
end
