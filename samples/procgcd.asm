.model      small
.code      
        push	br1
	push	br2
	call gcd
	pop rez
end1:
	print_num rez
	nop

gcd	proc
	pop ax ;mov ax,[bp+8]
	pop bx ;mov bx,[bp+6]
compare:    
            cmp   ax,bx
            je    endp
            cmp   ax,bx
            ja    greater
            sub   bx,ax
            jmp   compare

greater:
            sub   ax,bx
            jmp   compare

endp:
	push ax ;result
	    ret
gcd	endp

.data
	br1 dw 12
	br2 dw 8
	rez dw 0 
.stack
end
