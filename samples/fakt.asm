; Evolucija Softvera
; Racunanje faktoriela pomocu sateka i sabiranja

.286
.model small

sseg	segment stack
	db 256 dup (?)
sseg	ends

.data
n	dw  5		; racunamo faktoriel od 7
niz	db  5,4,3,2,1

.code
        mov dx, @data
        mov ds, dx
        ; Direktno indeksno - pomocu labela[DI] i labela[SI]
	;rezultat u dx
	mov ax, 0
        mov bx, 0
	mov cx,0
	mov dx,0
        mov dl, niz[bx]	;ubacuje najveci clan n koji ce biti sabran n-1 put
	inc bx		;krece od drugog clana	
	
petlja:
        mov cl, niz[bx]     ; citaj clan niza
	cmp cx,1	; da li je ax=1?
        je kraj		; ako jeste, idi na kraj
	
ubaci: 			;ubacuje na stek prethodni rezultat n-1 put
	cmp cx,1
	je pom
	push dx
	dec cx
	jmp ubaci
	
pom:	
	mov cl, niz[bx]	;uzima vrednost da koliko puta skida sa steka
	
mnozenje:
	cmp cx,1
	je sledeci
	pop ax
	add dx,ax
	dec cx
	jmp mnozenje
	
sledeci:    
        inc bx
	jmp petlja

kraj:
	print_num dx ;ispisi rezultat na kraju
        nop
        end
