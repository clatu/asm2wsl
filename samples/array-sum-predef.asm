.model small
;make a sum of an array - version with predefined everything

;#macros-removed-from-listing

.code
start:
        mov dx, @data
        mov ds, dx 
        mov cx, n
        mov ax, 0
        mov dx, 0
mainloop:              
        mov bx, cx
        add al, niz[bx-1]   ; get array memeber, byte
        ; store sum in al
        loop mainloop         ; end calc if done
        print_num ax
        end_execution
               
data segment
n       dw  7
niz     db  1,2,3,4,5,6,7,0     
tempStr db "        "

ends
end start
