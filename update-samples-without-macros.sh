#!/bin/bash
# copy from samples-with-macros and remove the macros segment in the samples folder

cp samples-with-macros/*.asm samples/
sed -i \
 -e '/#macro-commands-start/,/#macro-commands-end/ c ;#macros-removed-from-listing' \
 -e '/#extra-start/,/#extra-end/ d' \
 samples/*.asm